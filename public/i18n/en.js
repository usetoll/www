enTranslate = {
    "title": "UseToll",
    "subtitle": "Put crypto-paid pages on any website",
    "subsubtitle": "UseToll will enable you to ask for a crypto subscription by hour on any pages of your website",
    "quote": "\"UseToll disrupts traditional subscription by enabling true micro-payments, ideal for scenarios where users only need temporary access.\"",
    "signin": "Sign In",
    "signup": "Sign up",
    "keypoint-1-title": "Work everywhere",
    "keypoint-1-description": "Start by creating an account today. Nothing to install.",
    "keypoint-2-title": "Customisable",
    "keypoint-2-description": "Choose price per hour, cryptocurrencies and pages to monetize.",
    "keypoint-3-title": "Anonymous",
    "keypoint-3-description": "Webmaster and the visitor remain completely anonymous.",
    "keypoint-4-title": "Paid as you go",
    "keypoint-4-description": "Allow user to chose hourly subscription. True seamless pay wall.",
    "tutorial-title": "Accept crypto subscription in minutes. No KYC",
    "tutorial-description": "1. Create an account.<br>" +
        "2. Set up your Zone DNS with UseToll provided data.<br>" +
        "3. Go to your dashboard to configure your crypto address, price per hour, etc.<br>" +
        "4. Done ✅",
    "footer": "© usetoll 2024. All Right Reserved",
}
